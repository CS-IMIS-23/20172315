package cn.edu.besti.cs1723.HZT2315;

import java.io.*;
import java.util.Arrays;
import java.util.Scanner;

public class test {
    public static void main(String[] args) throws IOException {
        File file = new File("E:input.txt");
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        String temp = bufferedReader.readLine();

        char characters[] = new char[temp.length()];
        for (int i = 0; i < temp.length(); i++) {
            characters[i] = temp.charAt(i);
        }
        System.out.println("原字符集为：" + Arrays.toString(characters));
        //计算每一个字符出现的频率。用嵌套循环来实现，并把出现的概率存在另一个数组中
        double frequency[] = new double[27];
        int numbers = 0;//空格的个数
        for (int i = 0; i < characters.length; i++) {
            if (characters[i] == ' ') {
                numbers++;
            }
            frequency[26] = (float) numbers / characters.length;
        }
        System.out.println("字符集为");
        for (int j = 97; j <= 122; j++) {
            int number = 0;//给字母计数
            for (int m = 0; m < characters.length; m++) {
                if (characters[m] == (char) j) {
                    number++;
                }
                frequency[j - 97] = (float) number / characters.length;
            }
            System.out.print((char) j + "，");
        }
        System.out.println("空");
        System.out.println("每一个字符对应的概率为（从a到z还有一个空格）" + "\n" + Arrays.toString(frequency));






        StringBuffer buffer = new StringBuffer();
        BufferedReader bf= new BufferedReader(new FileReader("E:input.txt"));
        String s = null;
        while((s = bf.readLine())!=null){//使用readLine方法，一次读一行
            buffer.append(s.trim());
        }

        String xml = buffer.toString();
        Huffman huff = new Huffman();// 创建哈弗曼对象


        huff.creatHfmTree(xml);// 构造树

        huff.output(); // 显示字符的哈夫曼编码

        // 将目标字符串利用生成好的哈夫曼编码生成对应的二进制编码
        String hufmCode = huff.toHufmCode(xml);
        System.out.println("编码:" + hufmCode);

        // 将上述二进制编码再翻译成字符串
        System.out.println("解码：" + huff.CodeToString(hufmCode));


        File file1 = new File("E:file1.txt");
        FileWriter fileWriter = new FileWriter(file1);
        fileWriter.write(hufmCode);
        fileWriter.close();

    }

}
