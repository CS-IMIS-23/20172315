import java.util.Iterator;
import java.util.Stack;
import java.util.StringTokenizer;

public class InfixToSuffix
{
    private Stack<LinkedBinaryTree> linkedBinaryTreeStack = new Stack<LinkedBinaryTree>();
    private Stack a = new Stack();
    private int value= 0;

    private boolean fuhao(String token) {
        return (token.equals("+") || token.equals("-") || token.equals("*") || token
                .equals("/"));
    }

    public String infixToSuffix(String s)
    {
        String result="";
        StringTokenizer stringTokenizer = new StringTokenizer(s);

        while (stringTokenizer.hasMoreTokens())
        {
            String m = stringTokenizer.nextToken();
            if (fuhao(m)) {

                if (m.equals("*") || m.equals("/"))
                    a.push(m);
                else if (a.empty())
                    a.push(m);
                else {
                    while (!a.isEmpty()) {
                        String s1 = String.valueOf(a.pop());
                        LinkedBinaryTree operand3 = linkedBinaryTreeStack.pop();
                        LinkedBinaryTree operand4 = linkedBinaryTreeStack.pop();
                        LinkedBinaryTree<String> linkedBinaryTree1 = new LinkedBinaryTree<String>(s1, operand4, operand3);
                        linkedBinaryTreeStack.push(linkedBinaryTree1);
                        if (a.isEmpty())
                            break;
                    }
                    a.push(m);
                }
            }


            else
            {
                LinkedBinaryTree<String> linkedBinaryTree3 = new LinkedBinaryTree<String>(m);
                linkedBinaryTreeStack.push(linkedBinaryTree3);
            }
        }
        value = a.size();
        for (int y = 0; y < value; y++)
        {
            String s2  = String.valueOf(a.pop());
            LinkedBinaryTree operand5 = linkedBinaryTreeStack.pop();
            LinkedBinaryTree operand6 = linkedBinaryTreeStack.pop();
            LinkedBinaryTree<String> linkedBinaryTree1 = new LinkedBinaryTree<String>(s2,operand6,operand5);
            linkedBinaryTreeStack.push(linkedBinaryTree1);
        }

        LinkedBinaryTree linkedBinaryTree =  linkedBinaryTreeStack.pop();

        Iterator iterator = linkedBinaryTree.iteratorPostOrder();
        while (iterator.hasNext())
            result += iterator.next()+ " ";

        return result;
    }


}
