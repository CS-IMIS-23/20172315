import java.util.HashMap;


public class Searching {

   public static int BinarySearch2(int a[], int value, int low, int high)
    {
        int mid = low+(high-low)/2;
        if(a[mid]==value)
            return mid;
        if(a[mid]>value)
            return BinarySearch2(a, value, low, mid-1);
        if(a[mid]<value)
            return BinarySearch2(a, value, mid+1, high);
        return mid;
    }
    public static int sequentialSearch2(int[] a, int key) {
        int index = a.length - 1;
        a[0] = key;// 将下标为0的数组元素设置为哨兵
        while (a[index] != key) {
            index--;
        }
        return index;
    }

    public static int interpolationSearch(int[] a, int key) {
        int low, mid, high;
        low = 0;// 最小下标
        high = a.length - 1;// 最大小标
        while (low < high) {
            mid = low + (high - low) * (key - a[low]) / (a[high] - a[low]);
            // mid = (high + low) / 2;// 折半下标
            if (key > a[mid]) {
                low = mid + 1; // 关键字比 折半值 大，则最小下标 调成 折半下标的下一位
            } else if (key < a[mid]) {
                high = mid - 1;// 关键字比 折半值 小，则最大下标 调成 折半下标的前一位
            } else {
                return mid; // 当 key == a[mid] 返回 折半下标
            }
        }
        return -1;
    }

    public static int FibonacciSearch(int[] a, int n, int key) {
        int[] F = {0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
        int low, high, mid, i, k;
        low = 1;
        high = n;
        k = 0;
        while (n > F[k] - 1) /* 计算n位于斐波那契数列的位置 */
            k++;

        while (low <= high) {
            mid = low + F[k - 1] - 1;
            if (key < a[mid]) {
                high = mid - 1;
                k = k - 1;
            } else if (key > a[mid]) {
                low = mid + 1;
                k = k - 2;
            } else {
                if (mid <= n)
                    return mid;
                else
                    return n;
            }
        }
        return 0;
    }
    public static boolean BinaryTreeSearch(Comparable [] data, Comparable target) throws NonComparableElementException {
        LinkedBinarySearchTree tree = new LinkedBinarySearchTree();
        for (int a = 0; a< data.length; a++){
            tree.addElement(data[a]);
        }
        if (tree.find(target) == target)
            return true;
        else
            return false;
    }


    public static int Hashsearch(int[] hash, int Length, int key) {
        int Address = key % Length;

        while (hash[Address] != 0 && hash[Address] != key) {
           Address = (++Address) % Length;
        }
        if (hash[Address] == 0)
            return -1;
        return Address;
    }



    public static int binarySearch1(int[] array, int searchKey) {

        int low = 0;
        int high = array.length - 1;
        while (low <= high) {
            int middle = (low + high) / 2;
            if (searchKey == array[middle]) {
                return middle;
            } else if (searchKey < array[middle]) {
                high = middle - 1;
            } else {
                low = middle + 1;
            }
        }
        return -1;
    }
    public static int blockSearch(int[] index, int[] st, int key, int m) {

        int i = binarySearch1(index, key);
        if (i >= 0) {
            int j = i > 0 ? i * m : i;
            int len = (i + 1) * m;
            // 在确定的块中用顺序查找方法查找key
            for (int k = j; k < len; k++) {
                if (key == st[k]) {
                    System.out.println("查询成功");
                    return k;
                }
            }
        }
        System.out.println("查找失败");
        return -1;
    }






}

