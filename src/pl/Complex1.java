/**
 * @author 20172315
 */
public class Complex1 {
    double RealPart;
    double ImagePart;

    public Complex1(double R,double I){
        RealPart = R;
        ImagePart = I;
    }
    public void setRealPart(double a){
        RealPart = a;
    }
    public void setImagePart(double a){
        ImagePart = a;
    }
    public double getRealPart(){
        return RealPart;
    }
    public double getImagePart(){
        return ImagePart;
    }

    public boolean equals(double a,double b){
        if (RealPart == a && ImagePart == b) {
            return true;
        } else {
            return false;
        }
    }
    @Override
    public String toString(){
        if (ImagePart > 0) {
            return RealPart + "+" + ImagePart + "i";
        } else if (ImagePart == 0) {
            return RealPart + "+" + ImagePart + "i";
        } else {
            return RealPart + "" + ImagePart + "i";
        }

    }

    public String ComplexAdd(Complex a){
        ImagePart= ImagePart+a.getImagePart();
        RealPart= RealPart+a.getRealPart();
        Complex c = new Complex(RealPart,ImagePart);
        return c+"";
    }
    public String  ComplexSub(Complex a) {
        ImagePart = ImagePart - a.getImagePart();
        RealPart = RealPart - a.getRealPart();
        Complex c = new Complex(RealPart,ImagePart);
        return c+"";
    }

    public String  ComplexMulti(Complex a){
        double R,I;
        R = (RealPart*a.getRealPart())-(ImagePart*a.getImagePart());
        I = (RealPart*a.getImagePart())+(ImagePart*a.getRealPart());
        Complex c = new Complex(R,I);
        return c+"";
    }
    public String  ComplexDiv(Complex a){
        RealPart = (RealPart*a.getRealPart()+a.getImagePart()*ImagePart)/(Math.pow(a.getRealPart(),2)+Math.pow(a.getImagePart(),2));
        ImagePart =(ImagePart*a.getRealPart()-RealPart*a.getImagePart())/(Math.pow(a.getRealPart(),2)+Math.pow(a.getImagePart(),2));
        Complex c = new Complex(RealPart,ImagePart);
        return c+"";
    }
}
