public class DequeADT {
    public interface QueueADT<T> {
        public void enqueueFirst(T element);
        public void enqueueLast(T element);
        public T dequeueFirst();
        public T dequeueLast();
        public T first();
        public T Last();
        public boolean isEmpty();
        public int size();
        public String toString();

    }

}
