public class LindedStack<T> implements StackADT<T> {
    private int count;
    private LinnearNode<T> top;
    public LindedStack(){
        count=0;
        top=null;
    }

    @Override
    public void push(T element) {
        LinnearNode<T> temp=new LinnearNode<T>(element);

        temp.setNext(top);
        top = temp;
        count++;
    }

    @Override
    public T pop() throws EmptyCollectionException {

        if (isEmpty())
            throw new EmptyCollectionException("Stack");

        T result = top.getElement();
        top=top.getNext();

        count--;

        return result;
    }

    @Override
    public T peek() {

        if (isEmpty())
            throw new EmptyCollectionException("Stack");
        T result = top.getElement();
        top=top.getNext();

        return result;
    }

    @Override
    public boolean isEmpty() {

        boolean p ;
        if (size()==0){
            p=true;
        }
        else
        {
            p=false;
        }
        return p;
    }

    @Override
    public int size() {

        return count;
    }
    @Override
    public  String toString(){

        String result ="";
        LinnearNode<T> temp;
        temp = top;
        while(temp!=null){
            result+=temp.getElement()+" ";
            temp=temp.getNext();
        }
        return result;
    }


}

