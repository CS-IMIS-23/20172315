import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

/**
 * LinkedQueue represents a linked implementation of a queue.
 * 
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedQueue<T> implements QueueADT<T>
{
    private int nHuZhitao;
    private LinearNode<T> head, tail;

    /**
     * Creates an empty queue.
     */
    public LinkedQueue()
    {
        nHuZhitao = 0;
        head = tail = null;
    }

    /**
     * Adds the specified element to the tail of this queue.
     * @param element the element to be added to the tail of the queue
     */
    public void enqueue(T element)
    {
        LinearNode<T> node = new LinearNode<T>(element);

        if (isEmpty())
            head = node;
        else
            tail.setNext(node);

        tail = node;
        nHuZhitao++;
    }

    /**
     * Removes the element at the head of this queue and returns a
     * reference to it. 
     * @return the element at the head of this queue
     * @throws EmptyCollectionException if the queue is empty
     */
    public T dequeue() throws EmptyCollectionException
    {
        if (isEmpty())
            throw new EmptyCollectionException("queue");

        T result = head.getElement();
        head = head.getNext();
        nHuZhitao--;

        if (isEmpty())
            tail = null;

        return result;
    }
   
    /**
     * Returns a reference to the element at the head of this queue.
     * The element is not removed from the queue.  
     * @return a reference to the first element in this queue

     */
    public T first() throws EmptyCollectionException
    {
        if(isEmpty())
            throw new EmptyCollectionException("queue");
else
            return head.getElement();

        // To be completed as a Programming Project
    }

    /**
     * Returns true if this queue is empty and false otherwise. 
     * @return true if this queue is empty 
     */
    public boolean isEmpty()
    {
        if (nHuZhitao == 0)
            return true;
        else
            return false;

        // To be completed as a Programming Project
    }
 
    /**
     * Returns the number of elements currently in this queue.
     * @return the number of elements in the queue
     */
    public int size()
    {
        return nHuZhitao; // To be completed as a Programming Project
    }

    public void insert(T element, int n )throws Exception
    {

        LinearNode<T> node1 = new LinearNode<T>(element);
        LinearNode<T> current = head;
        if(n==0){
            node1.setNext(current);
            head = node1;
        }else {
            for (int i = 0; i < n - 1; i++)
                current = current.getNext();
            node1.setNext(current.getNext());
            current.setNext(node1);
        }


        nHuZhitao++;
    }

    public void delete(int n){
        LinearNode pre, temp = null;
        if (isEmpty())
            throw new EmptyCollectionException("Queue is empty.");

        if( n == 0) {
            temp = head;
            head = head.getNext();
            temp.setElement(null);
        }else{
            pre = head;
            temp = head.getNext();
            for(int i = 0; i < n; i ++) {
                temp = pre;
                pre = pre.getNext();
            }
            temp.setNext(pre.getNext());

        }

        nHuZhitao --;
    }



    public static  String sort(String s){
        String result="";
        StringTokenizer stringTokenizer = new StringTokenizer(s);
        int [] score = new int[stringTokenizer.countTokens()];
        for (int i =0;stringTokenizer.hasMoreTokens();i++){
            score[i]=Integer.parseInt(stringTokenizer.nextToken());
        }
        for(int i =0;i < score.length - 1;i++){//使用冒泡排序
            for(int j = 0;j <  score.length - 1-i;j++){ // j开始等于0，
                if(score[j] < score[j+1]) {
                    int temp = score[j];
                    score[j] = score[j+1];
                    score[j+1] = temp;
                }
            }
        }
        for (int i =0 ;i<score.length;i++){
            result+= score[i]+ " ";
        }
        return result ;
    }











    public String toString()
    {
        LinearNode<T> current = head;
        String result = "";
        int a = nHuZhitao;
        while(a > 0) {
            result += current.getElement()+ " ";
            current = current.getNext();
            a--;
        }

        return result;
    }




}
