import java.util.Scanner;
import java.util.StringTokenizer;
import java.io.*;

public class shiyan5 {
    public static void main(String[] args) throws Exception {
        String a;
        char[] A = new char[10];
        Scanner scan1 = new Scanner(System.in);
        System.out.println("请输入整数");
        a = scan1.nextLine();
        ArrayStack array = new ArrayStack();
        StringTokenizer b = new StringTokenizer(a);

        while (b.hasMoreTokens()) {
            array.push(b.nextToken());
        }

        System.out.println("打印所有链表元素：" + array);
        System.out.println("元素的总数为：" + array.size());


        File file = new File("Hello1.txt");
        // 创建文件
        file.createNewFile();
        // creates a FileWriter Object
        FileWriter writer = new FileWriter(file);
        String c = "1 2 ";
        writer.write(c);
        writer.flush();
        writer.close();
        System.out.println("从文件中读入数字1，插入到链表第 5 位");
        StringTokenizer str = new StringTokenizer(c, " ");
        int num1 = (Integer.parseInt(str.nextToken()));
        array.insert(num1,4 );
        System.out.println(array.toString());
        System.out.println("所有元素数量元素:" + array.size());
        System.out.println("从文件中读入数字2， 插入到链表第 0 位");
        int num2 = (Integer.parseInt(str.nextToken()));
        array.insert(num2, 0);
        System.out.println(array.toString());
        System.out.println("元素的总和:" + array.size());

        System.out.println("从链表中删除刚才的数字1.  并打印所有数字和元素的总数");
        array.delete(5);


        System.out.println(array.toString());
        System.out.println("元素的总和:" + array.size());
        String d =array.toString();

        System.out.println("排序后的顺序："+array.sort(d));
    }
}