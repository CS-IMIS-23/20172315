import java.util.Scanner;
public class pp121 {
    public static void main(String[] args) {
        String str, another = "y";

        Scanner scan = new Scanner(System.in);
        while (another.equalsIgnoreCase("y")) {
            System.out.print("Enter a potential palindrome:");
            str = scan.nextLine();
            if (huiwen(str))
                System.out.println("That string IS a palindrome.");
            else
                System.out.println("That string is NOT a palindrome.");

            System.out.println();
            System.out.println("Test another parlindrome(y/n)?");
            another = scan.nextLine();
        }
    }

    static boolean huiwen(String str) {
        int left = 0;
        int right = str.length() - 1;
        if (str.length() == 1)
            return true;
        else {
            if (str.charAt(left) != str.charAt(right) ) {
                return false;
            } else
                str = str.substring(1, str.length() - 1);
            return huiwen(str);

        }
    }
}

