public class MagazineRack {
    public static void main(String[] args) {
        MagazineList rack = new MagazineList();

        rack.add(new Magazine("Time"));
        rack.add(new Magazine("Woodworking Today"));
        rack.add(new Magazine("Communications of the ACM"));
        rack.add(new Magazine("House and Garden"));
        rack.add(new Magazine("GQ"));

        Magazine qqq = new Magazine("play");
        rack.insert(2, qqq);
        System.out.println(rack);
        rack.delete(qqq);
        System.out.println(rack);

    }
}
