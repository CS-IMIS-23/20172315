public class ceshi1 {
    public static void main(String[] args) {
        LinkedOrderedList a = new LinkedOrderedList();
        a.add(20);
        a.add(17);
        a.add(23);
        a.add(15);
        System.out.println(a);
        a.add(4);
        System.out.println(a);
        System.out.println("The first is " + a.first());
        System.out.println("The last is " + a.last());
        System.out.println("The size is " + a.size());
        a.remove(4);
        System.out.println(a.contains(4));
        System.out.println(a.contains(15));
        a.removeFirst();
        System.out.println(a);
    }
}
