public class shiyan1 {
    public static void main(String[] args) throws NonComparableElementException {
        ArrayOrderedList a = new ArrayOrderedList(10);
        a.add(20);
        a.add(17);
        a.add(23);
        a.add(15);
        System.out.println(a);
        a.add(3);
        System.out.println(a);
        System.out.println("The first is " + a.first());
        System.out.println("The last is " + a.last());
        System.out.println("The size is " + a.size());
        a.remove(3);
        System.out.println(a.contains(3));
        System.out.println(a.contains(1));
        a.removeFirst();
        a.removeLast();
        System.out.println(a);
    }
}
