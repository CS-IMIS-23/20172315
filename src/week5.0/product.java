public class product implements Comparable<product> {
    private String name;
    private int price;
    private int quantity;

    public product(String name, int price, int quantity) {
        super();
        this.name = name;
        this.price = price;
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return price;
    }

    public void setQuantity(int price) {
        this.price = price;
    }

    public String toString() {
        String result = name + " " + price + "元" + " "+ "剩余" + quantity + "件";
        return result;
    }

    public int compareTo(product o) {
        if (name.compareTo(o.name) > 0) {
            return 1;
        }
        if (name.compareTo(o.name) < 0) {
            return -1;
        }
        if (name.compareTo(o.name) == 0) {
            if (quantity > o.quantity) {
                return 1;
            }
            if (quantity < o.quantity) {
                return -1;
            }
            if (quantity == o.quantity) {
                if (quantity > o.quantity) {
                    return 1;
                }
            }

        }
        return 0;
    }
}