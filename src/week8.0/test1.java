public class test1
{
    public static void main(String[] args) {
        int[] a ={36,30,18,40,32,45,22,50};
        int b[]=new int[8];
        ArrayHeap<Integer> ArrayHeap = new ArrayHeap<>();
        for (int i = 0;i<a.length;i++)
        {
            ArrayHeap.addElement(a[i]);
        }
        System.out.println("大顶堆序列: "+ ArrayHeap);

        System.out.println("每轮排序中数组的结果");
        int n= 0;
        for (int i = 0;i<a.length;i++)
        {
            n ++;
            b[i] = ArrayHeap.removeMax();
            System.out.println("第"+n+"次:"+ArrayHeap);
        }
        System.out.println("最终的排序结果为：");
        for (int i= 0;i<b.length;i++)
            System.out.print(b[i]+" ");
    }
}

