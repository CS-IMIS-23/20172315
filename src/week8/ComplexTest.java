public class ComplexTest extends TestCase {

      Complex a = new Complex(1,2);
      Complex b = new Complex(1,3);
      Complex c = new Complex(2,2);
      Complex d = new Complex(2,3);
      Complex e = new Complex(1,0);

    @Test
 public void testComplexAdd() throws Exception
 {
   assertEquals(2.0+"+5.0i",String.valueOf(a.ComplexAdd(b)));
 }

    @Test
 public void testComplexSub() throws Exception
 {
   assertEquals(-1.0+"+1.0i",String.valueOf(b.ComplexSub(c)));
   }

    @Test
 public void testComplexMulti() throws Exception
 {
  assertEquals(-2.0+"+10.0i",String.valueOf(c.ComplexMulti(d)));
    }

         @Test
     public void testComplexDiv() throws Exception
  {
       assertEquals(2.0+"+3.0i",String.valueOf(d.ComplexDiv(e)));
      }
																											    }
