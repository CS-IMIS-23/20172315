
public class Nodea<T> {
    public T element;
    public Nodea<T> next;


    public Nodea(T element)  // constructor
    {
        this.element = element;
        next = null;
    }


    //为节点添加邻接点
    public void setNext(Nodea<T> a){
        next = a;
    }

    public Nodea<T> getNext(){
        return next;
    }

    public T getElement(){
        return element;
    }


}
